# Change Log


## [Version 3] 2021-11-25
    ### Add
    	-MundoCliente.cpp 
    	-MundoCliente.h
    	-MundoServidor.cpp
    	-MundoServidor.h
    	-servidor.cpp
    	-cliente.cpp
    	-Coordenadas.h
    	
    ### Change
        Adición de ejecutables de servidor y cliente.
        Se han borrado los ficheros relacionados con el mundo y el tenis.
        En esta versión:
        	-El servidor se comunica con logger a través de una FIFO. 
        	-Cliente se comunica con el bot a través de proyección de memoria compartida.
        	-Servidor manda las coordenadas de la esfera y de las raquetas y los puntos al cliente a través de una FIFO.
        	-Cliente manda las teclas pulsadas por el usuario a un hilo del servidor a través de una FIFO.
        
## [Version 2] 2021-11-11
    ### Add
    	He creado los ficheros:
    	-logger.cpp
    	-bot.cpp
    	-DatosMemCompartida.h
    	-Mensaje.h
    ### Change
        Adición de ejecutables logger y bot en CMakeLists.txt.
        
## [Version 1.1] 2021-10-28
    ### Add
    	He creado el fichero README.
    ### Change
    	He añadido movimiento a la esfera y a la raqueta.
    	He añadido reducción de tamaño de esfera.

## [Version 1.0] 2021-10-07
    ### Add
        He creado el fichero Changelog.
    ### Change
    	He añadido el autor a los ficheros fuente.
