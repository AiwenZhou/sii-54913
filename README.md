# JUEGO DE TENIS 21/22

** Autora: Aiwen Zhou **

## Instrucciones

1. Las raquetas se mueven verticalmente con el teclado del ordenador. 
	Para mover la raqueta 1: usar teclas 'w' y 's'. 
	Para mover raqueta 2: usar teclas 'o' y 'l'
2. La pelota va disminuyendo de tamaño a medida que avanza el juego.
3. La raqueta del jugador 1 puede ser controlada por un bot (abrir ejecutable bot).




