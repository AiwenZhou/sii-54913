// Mundo.cpp: implementation of the CMundo class.
//Autor: Aiwen Zhou
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
	estado=JUEGO;
}

CMundo::~CMundo()
{
	close(fd);
	close(id_tub_cliente);
	close(id_tub_teclas);
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);

	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++)
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	
	//Borrado de la pantalla
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	
	if(estado==FIN){
		char mFIN[100];
   		if(puntos1>puntos2) sprintf(mFIN,"FIN: Ha ganado el jugador1");
   		else if(puntos2>puntos1) sprintf(mFIN,"FIN: Ha ganado el jugador2");
		print(mFIN,300,0,1,1,1);
	
	}
	
	else if(estado==JUEGO){
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	}
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
	

}

void CMundo::OnTimer(int value)
{
	Mensaje mensaje;
	Coordenadas infoCoord;
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	esfera.ReduceTamano(0.025);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		//Escribe el mensaje por la tubería
		mensaje.id=2;
		mensaje.total=puntos2;
		write(fd, &mensaje, sizeof(mensaje));
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		//Escribe el mensaje por la tubería
		mensaje.id=1;
		mensaje.total=puntos1;
		write(fd, &mensaje, sizeof(mensaje));
	}
	

	
	//Fin del juego si punto=3
	if(puntos1>=3 || puntos2>=3)
		estado=FIN;

	///Escritura en FIFO CLIENTESERV
	/*infoCoord.Coordx_esfera=esfera.centro.x;
	infoCoord.Coordy_esfera=esfera.centro.y;
	infoCoord.Coordx1_raqueta1=jugador1.x1;
	infoCoord.Coordy1_raqueta1=jugador1.y1;
	infoCoord.Coordx2_raqueta1=jugador1.x2;
	infoCoord.Coordy2_raqueta1=jugador1.y2;
	infoCoord.Coordx1_raqueta2=jugador2.x1;
	infoCoord.Coordy1_raqueta2=jugador2.y1;
	infoCoord.Coordx2_raqueta2=jugador2.x2;
	infoCoord.Coordy2_raqueta2=jugador2.y2;*/
	
	infoCoord.esfera=esfera;
	infoCoord.raqueta1=jugador1;
	infoCoord.raqueta2=jugador2;
	infoCoord.puntos1=puntos1;
	infoCoord.puntos2=puntos2;
	write(id_tub_cliente, &infoCoord, sizeof(infoCoord));
	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
//	switch(key)
//	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
//	case 's':jugador1.velocidad.y=-4;break;
//	case 'w':jugador1.velocidad.y=4;break;
//	case 'l':jugador2.velocidad.y=-4;break;
//	case 'o':jugador2.velocidad.y=4;break;

//	}
}
void* hilo_comandos(void* d)
{
  	
      CMundo* p=(CMundo*) d;
      p->RecibeComandosJugador();
      return nullptr;
}

void CMundo::RecibeComandosJugador()
{
     while (1) {
     
     
            usleep(10);
            char cad[100];
   //         printf("hola");
            read(id_tub_teclas, cad, sizeof(cad));
     //       printf("%c",*cad);
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	
	
	//abrir FIFO
	if ((fd=open("FIFO", O_WRONLY))<0) {
        perror("No puede abrirse el FIFO");
        }
        else printf("FIFO LOGGER abierto :)\n");
        
        
        //abrir FIFO SERVIDORCLIENTE
	 if ((id_tub_cliente=open("CLIENTESERV", O_WRONLY))<0) {
        perror("No puede abrirse el FIFO CLIENTE-SERVIDOR");
        }
        else printf("FIFO CLIENTESERV abierto :)\n");
        
        if ((id_tub_teclas=open("CLIENTEHILO", O_RDONLY))<0) {
        perror("No puede abrirse el FIFO CLIENTE-HILO(SERVIDOR)");
 
        }
   	else printf("FIFO CLIENTE-HILO(SERVIDOR) abierto :)\n");
      
        //Creación hilo
        pthread_create(&thid, NULL, hilo_comandos, this);
	  
           
}


