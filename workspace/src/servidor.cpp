//Autor: Aiwen Zhou
#ifndef FIcheros
#include "glut.h"
#include "MundoServidor.h"
#endif

#include <signal.h>

//el unico objeto global
CMundo mundo;

//los callback, funciones que seran llamadas automaticamente por la glut
//cuando sucedan eventos
//NO HACE FALTA LLAMARLAS EXPLICITAMENTE
void OnDraw(void); //esta funcion sera llamada para dibujar
void OnTimer(int value); //esta funcion sera llamada cuando transcurra una temporizacion
void OnKeyboardDown(unsigned char key, int x, int y); //cuando se pulse una tecla	
struct sigaction acc;
void capturaSenales(int s) {
	
	//Establecer función señal por defecto (matar tras capturar numero)
	kill (getpid(),s);
	printf("Termina el proceso por recepción de senal %d\n", s);
        acc.sa_handler = SIG_DFL;
	acc.sa_flags=0;
	sigemptyset(&acc.sa_mask);
	sigaction(SIGINT, &acc, NULL);
}

int main(int argc,char* argv[])
{
	sigset_t mascaraResto;
	
	struct sigaction acc2;
	
	//Captura número de señal que mata el proceso
	acc.sa_handler = capturaSenales;
        acc.sa_flags=0;
        sigemptyset(&acc.sa_mask);
        sigaction(SIGINT, &acc, NULL);
        sigaction(SIGTERM, &acc, NULL);
	 sigaction(SIGPIPE, &acc, NULL);
	
        ///Ignora señal SIGUSR2
        acc2.sa_handler = SIG_IGN;
        acc2.sa_flags=0;
        sigemptyset(&acc2.sa_mask);
        sigaction(SIGUSR2, &acc2, NULL);

/////////////////////////////////////////////////////////////////////////////
	//Inicializar el gestor de ventanas GLUT
	//y crear la ventana
	glutInit(&argc, argv);
	glutInitWindowSize(800,600);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	glutCreateWindow("Mundo");


	//Registrar los callbacks
	glutDisplayFunc(OnDraw);
	//glutMouseFunc(OnRaton);

	glutTimerFunc(25,OnTimer,0);//le decimos que dentro de 25ms llame 1 vez a la funcion OnTimer()
	//glutKeyboardFunc(OnKeyboardDown);
	glutSetCursor(GLUT_CURSOR_FULL_CROSSHAIR);
	
	mundo.InitGL();

	
	//pasarle el control a GLUT,que llamara a los callbacks
	glutMainLoop();

	return 0;   
}

void OnDraw(void)
{
	mundo.OnDraw();
}
void OnTimer(int value)
{
	mundo.OnTimer(value);
	glutTimerFunc(25,OnTimer,0);
	glutPostRedisplay();
}
void OnKeyboardDown(unsigned char key, int x, int y)
{
	mundo.OnKeyboardDown(key,x,y);
	glutPostRedisplay();
}
