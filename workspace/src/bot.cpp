#ifndef DATOS
#include "DatosMemCompartida.h"
#endif

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <ctype.h>
#include <ctime>
using namespace std;

int main()
{	DatosMemCompartida* ptrdatos=NULL;
	int fd_memc;
	struct stat bstat;
	
	//Abrir fichero de memoria compartida
	if((fd_memc=open("Datos",O_RDWR))<0){
        	fprintf(stderr,"Error al abrir fichero de memoria compartida");
        }

	//Proyeccion en memoria
	if ((fstat(fd_memc, &bstat))<0) {
		perror("Error en fstat del fichero Datos");
		close(fd_memc);//cerrar si error
	}
	
	ptrdatos=(DatosMemCompartida *)mmap(0,bstat.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd_memc,0);
	
	if(ptrdatos==(DatosMemCompartida *) MAP_FAILED){
		perror("Error en proyeccion del fichero");
		close(fd_memc);//cerrar si error
	}
	
	//Cerrar
	close(fd_memc);
	while(1){
	float esf_x=ptrdatos->esfera.centro.x;
	float esf_y=ptrdatos->esfera.centro.y;
	float raq1_y1=ptrdatos->raqueta1.y1;
	float raq1_y2=ptrdatos->raqueta1.y2;
	float esf_radio=ptrdatos->esfera.radio;

	
	usleep(25000);
	
	if((esf_y)>raq1_y1) ptrdatos->accion=1;
	else if (esf_y<raq1_y2) ptrdatos->accion=-1;
	else ptrdatos->accion=0;
	


	/* Se eliminan las proyecciones */
	//munmap(*ptrdatos, bstat.st_size);
	}
	munmap(ptrdatos,bstat.st_size);

	return 0;
}
