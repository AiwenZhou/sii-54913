// Mundo.cpp: implementation of the CMundo class.
//Autor: Aiwen Zhou
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
    Init();
    estado=JUEGO;
}

CMundo::~CMundo()
{
    	munmap(ptrdatos,bstat.st_size);
	close(id_tub);
	unlink("CLIENTESERV");
	close(id_tub_teclas);
	unlink("CLIENTEHILO");
}

void CMundo::InitGL()
{
    //Habilitamos las luces, la renderizacion y el color de los materiales
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);

    glMatrixMode(GL_PROJECTION);
    gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
    glDisable (GL_LIGHTING);

    glMatrixMode(GL_TEXTURE);
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    glColor3f(r,g,b);
    glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
    int len = strlen (mensaje );
    for (int i = 0; i < len; i++)
        glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );

    glMatrixMode(GL_TEXTURE);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
    
    //Borrado de la pantalla
       glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Para definir el punto de vista
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluLookAt(0.0, 0, 17,  // posicion del ojo
        0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0)
        0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)

    /////////////////
    ///////////
    //        AQUI EMPIEZA MI DIBUJO
    
    if(estado==FIN){
        char mFIN[100];
           if(puntos1>puntos2) sprintf(mFIN,"FIN: Ha ganado el jugador1");
           else if(puntos2>puntos1) sprintf(mFIN,"FIN: Ha ganado el jugador2");
        print(mFIN,300,0,1,1,1);
    
    }
    
    else if(estado==JUEGO){
    char cad[100];
    sprintf(cad,"Jugador1: %d",puntos1);
    print(cad,10,0,1,1,1);
    sprintf(cad,"Jugador2: %d",puntos2);
    print(cad,650,0,1,1,1);
    int i;
    for(i=0;i<paredes.size();i++)
        paredes[i].Dibuja();

    fondo_izq.Dibuja();
    fondo_dcho.Dibuja();
    jugador1.Dibuja();
    jugador2.Dibuja();
    esfera.Dibuja();
    }
    /////////////////
    ///////////
    //        AQUI TERMINA MI DIBUJO
    ////////////////////////////

    //Al final, cambiar el buffer
    glutSwapBuffers();
    

}

void CMundo::OnTimer(int value)
{
    Coordenadas infoCoord;

    //Gestion bot
    //Actualizacion datos
    ptrdatos->esfera=esfera;
    ptrdatos->raqueta1=jugador1;
    
    //Llamada a OnKeyBoardDown
    if(ptrdatos->accion==1){
        OnKeyboardDown('w', 0, 0);
    }
    else if(ptrdatos->accion==-1){
        OnKeyboardDown('s', 0, 0);
    }
    
    //Fin del juego si punto=3
    if(puntos1>=3 || puntos2>=3)
        estado=FIN;

	//LEER FIFO CLIENTESERV
	read(id_tub, &infoCoord, sizeof(infoCoord));
		esfera=infoCoord.esfera;
		jugador1=infoCoord.raqueta1;
		jugador2=infoCoord.raqueta2;
		puntos1=infoCoord.puntos1;
		puntos2=infoCoord.puntos2;

}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{   char cad[100];
    switch(key)
    {

    case 's':sprintf(cad,"%c",'s');
	     write(id_tub_teclas, &cad, sizeof(cad));
	     break;
    case 'w':sprintf(cad,"%c",'w');
	     write(id_tub_teclas, &cad, sizeof(cad));
	     break;
    case 'l':sprintf(cad,"%c",'l');
	     write(id_tub_teclas, &cad, sizeof(cad));
	     break;
    case 'o':sprintf(cad,"%c",'o');
	     write(id_tub_teclas, &cad, sizeof(cad));
	     break;

    }
    
}

void CMundo::Init()
{
    Plano p;
//pared inferior
    p.x1=-7;p.y1=-5;
    p.x2=7;p.y2=-5;
    paredes.push_back(p);

//superior
    p.x1=-7;p.y1=5;
    p.x2=7;p.y2=5;
    paredes.push_back(p);

    fondo_izq.r=0;
    fondo_izq.x1=-7;fondo_izq.y1=-5;
    fondo_izq.x2=-7;fondo_izq.y2=5;

    fondo_dcho.r=0;
    fondo_dcho.x1=7;fondo_dcho.y1=-5;
    fondo_dcho.x2=7;fondo_dcho.y2=5;

    //a la izq
    jugador1.g=0;
    jugador1.x1=-6;jugador1.y1=-1;
    jugador1.x2=-6;jugador1.y2=1;

    //a la dcha
    jugador2.g=0;
    jugador2.x1=6;jugador2.y1=-1;
    jugador2.x2=6;jugador2.y2=1;
    
    
  ////////CREACION TUBERIA CLIENTE-SERVIDOR///////////////////////////
    // borra FIFO por si existía previamente
    unlink("CLIENTESERV");
    
    /* crea el FIFO */
    if (mkfifo("CLIENTESERV", 0666)<0) {
        perror("No puede crearse el FIFO CLIENTE-SERVIDOR");
        return;
    }
   
   if ((id_tub=open("CLIENTESERV", O_RDONLY))<0) {
        perror("No puede abrirse el FIFO CLIENTE-SERVIDOR");
        return;
        }
    else printf("FIFO CLIENTESERV abierto :)\n");
    ////////////////////////////////////////////////////////////////////////
    
  ///////CREACION TUBERIA CLIENTE-HILOSERVIDOR TECLAS////////////////////////
    // borra FIFO por si existía previamente
    unlink("CLIENTEHILO");
    
    /* crea el FIFO */
    if (mkfifo("CLIENTEHILO", 0666)<0) {
        perror("No puede crearse el FIFO CLIENTE-HILO(SERVIDOR)");
        return;
    }
   
   if ((id_tub_teclas=open("CLIENTEHILO", O_WRONLY))<0) {
        perror("No puede abrirse el FIFOV CLIENTE-HILO(SERVIDOR)");
        return;
        }
   else printf("FIFO CLIENTE-HILO(SERVIDOR) abierto :)\n");
   //////////////////////////////////////////////////////////////////////////
   
   ///////////////////////MEMORIA COMPARTIDA/////////////////////////////////     
        //1. Crear fichero memoria compartida (open y write)
        int fd_memc;
        if((fd_memc=open("Datos",O_RDWR|O_CREAT|O_TRUNC,0666))<0){
            fprintf(stderr,"Error al crear fichero de memoria compartida");
        }
        write(fd_memc,&datos,sizeof(datos));
        
        //2. Proyectar el fichero de memoria(mmap)
        if ((fstat(fd_memc, &bstat))<0) {
        perror("Error en fstat del fichero origen");
        close(fd_memc);//cerrar si error
    }
    ptrdatos=(DatosMemCompartida *)mmap(0,bstat.st_size,PROT_READ|PROT_WRITE,MAP_SHARED,fd_memc,0);
    if(ptrdatos==(DatosMemCompartida *) MAP_FAILED){
        perror("Error en proyeccion de memoria");
        close(fd_memc);//cerrar si error
    }
        //3. Cerrar el descriptor de fichero
        close(fd_memc);
        //4. Asignar dir de comienzo de la region creada al atributo de tipo ptr creado en el paso2
        
   ////////////////////////////////////////////////////////////////////////////     
           
}

