// Esfera.cpp: implementation of the Esfera class.
//Autor: Aiwen Zhou
//////////////////////////////////////////////////////////////////////

#include "Esfera.h"
#include "glut.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Esfera::Esfera()
{
	radio=0.5f;
	velocidad.x=3;
	velocidad.y=3;
	centro.x=0;
	centro.y=0;
}

Esfera::~Esfera()
{

}



void Esfera::Dibuja()
{
	glColor3ub(255,255,0);
	glEnable(GL_LIGHTING);
	glPushMatrix();
	glTranslatef(centro.x,centro.y,0);
	glutSolidSphere(radio,15,15);
	glPopMatrix();
}

void Esfera::Mueve(float t)
{
	centro=centro+velocidad*t+aceleracion*(0.5f*t*t);//x=x0+v*t+0.5*a*t²
	velocidad=velocidad+aceleracion*t;//v=v0+a*t

}

void Esfera::ReduceTamano(float t){
	
	if(radio>0.25)radio=radio-0.001;


}
