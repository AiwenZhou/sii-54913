// Mundo.h: interface for the CMundo class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
#define AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_

#include <vector>
#include "Plano.h"
//#include "Raqueta.h"
#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Mensaje.h"
#include "Coordenadas.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000




#include <string>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <sys/mman.h>


using namespace std;

class CMundo  
{	int fd;
	int id_tub;
	int id_tub_teclas;
	DatosMemCompartida datos;
	DatosMemCompartida* ptrdatos=NULL;
	struct stat bstat;
	
public:
	enum ESTADO{JUEGO,FIN};
	ESTADO estado=JUEGO;
	void Init();
	CMundo();
	virtual ~CMundo();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	
	bool flag3puntos=0;
	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
};

#endif // !defined(AFX_MUNDO_H__9510340A_3D75_485F_93DC_302A43B8039A__INCLUDED_)
