// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////
#ifndef FICHEROS_
#define FICHEROS_
#include "Plano.h"
#include "Vector2D.h"
#endif


class Raqueta:public Plano  
{
public:
	//Vector2D posicion;
	Vector2D velocidad;
	//Vector2D aceleracion;

	Raqueta();
	virtual ~Raqueta();

	void Mueve(float t);

};
